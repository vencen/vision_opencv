vision_opencv
=============

.. image:: https://travis-ci.org/ros-perception/vision_opencv.svg?branch=indigo
    :target: https://travis-ci.org/ros-perception/vision_opencv

Packages for interfacing ROS with OpenCV, a library of programming functions for real time computer vision.

## 编译 cv_bridge 

### 安装环境

sudo apt-get install  python3-dev python3-numpy python3-yaml ros-melodic-cv-bridge python3-rospkg-modules

pip3 install --upgrade pip

pip3 install rosdep rosinstall catkin_pkg

### 基于python3环境编译cv_bridge

mkdir -p cv_bridge_ws/src && cd cv_bridge_ws/src

catkin_init_workspace

git clone https://gitee.com/vencen/vision_opencv.git

cd ../

catkin_make install -DPYTHON_EXECUTABLE=/usr/bin/python3

### 添加编译成功的软件环境变量

gedit ~/.bashrc # 底部添加：

source ~/cv_bridge_ws/install/setup.bash --extend

. ~/.bashrc # 更新.bashrc

或 不确定是否有效： sys.path.append("~/cv_bridge_ws/install/lib/python3/dist-packages/")

### 测试

import cv_bridge

from cv_bridge.boost.cv_bridge_boost import getCvType


